; $Id: colorbox.make,v 1.1.4.2 2010/06/29 05:36:26 frjo Exp $

core = 7.x

libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "http://colorpowered.com/colorbox/colorbox.zip"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][destination] = "libraries"
